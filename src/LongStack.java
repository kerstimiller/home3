import java.util.LinkedList;

public class LongStack {

   public static void main (String[] argum) {}

   private LinkedList stack;

   /**
    * Creates ne constructor for stack: LongStack()
    */
   LongStack() {
      stack = new LinkedList<>();
   }

   /**
    * Creates a copy: Object clone()
    * @return
    * @throws CloneNotSupportedException
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
     LongStack stackClone = new LongStack();
     stackClone.stack = (LinkedList)stack.clone();
     return stackClone;
   }

   /**
    * Checks if stack is empty: boolean stEmpty()
    * @return
    */
   public boolean stEmpty() { return (stack.size() <= 0); }

   /**
    * Adds element into stack: void push (long a)
    * @param a
    */
   public void push (long a) {
     try {
       stack.push(a);
     } catch(IndexOutOfBoundsException iobe){
        throw new IndexOutOfBoundsException("Too many items in stack, can't push anything to the stack");
     }
   }

   /**
    * Removes element from stack: long pop()
    * @return
    */
   public long pop() {
     long tmp = (long)stack.pop();
     try {
       return tmp;
     } catch (IndexOutOfBoundsException iobe) {
       if (stEmpty()) {
         throw new IndexOutOfBoundsException("Too few items in stack, can't pop anything from the stack");
       }
     }
     return tmp;
   }

   /**
   * Calculates value of two top most elements( + - * / ) Adds result as new top:
   * void op (String s)
   */
   public void op (String operand) {
      if (stack.size() < 2) {
         throw new IndexOutOfBoundsException("Too few elements for " + operand);
      }
      long op2 = pop();
      long op1 = pop();
      if (operand.equals("+"))
         push(op1 + op2);
      else if (operand.equals("-"))
         push(op1 - op2);
      else if (operand.equals("*"))
         push(op1 * op2);
      else if (operand.equals("/"))
         push(op1 / op2);
      else
         throw new IllegalArgumentException("Invalid operation: " + operand);
   }

   /**
    * Peeks the top: long tos()
    * @return
    */
   public long tos() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack you are trying to read is empty");
      }
     return (long)stack.peek();
   }

   /**
    * Compares two stacks: boolean equals (Object o)
    * @param o
    * @return
    */
   @Override
   public boolean equals (Object o) {
     LinkedList equalsTmp = ((LongStack) o).stack;
     if (equalsTmp.size() != stack.size()) {
       return false;
     }
     for (int i = 0; i < stack.size(); i++) {
       if (equalsTmp.get(i) != stack.get(i)) {
         return false;
       }
     }
     return true;
   }

   /**
    * Parses into string (top at the end): String toString()
    * @return
    */
   @Override
   public String toString() {
     StringBuffer stringBuf = new StringBuffer();
     if (stEmpty()) {
       return "empty";
     }
     for (int i = stack.size()-1; i > 0; i--) { // Take items from stack backwards
       stringBuf.append(stack.get(i) + " ");
     }
     return stringBuf.toString();
   }

   /**
   * Calculates input string and returns value (last item) or error message
   */
   public static long interpret (String pol) {
     if (pol.length() == 0) {
       throw new RuntimeException("Pol can't be empty.");
     }

     String[] items = pol.trim().split("\\s+");                  // Create String array of splitted elements
     LongStack stackInterpret = new LongStack();                        // new Longstack that can use our methods
     int numbers = 0;                                                   // Count numbers and operators
     int operators = 0;

     for (String item : items) {
       try {
         stackInterpret.push(Long.valueOf(item));
         numbers++;
       } catch (NumberFormatException nfe) {
         if (!item.equals("+") && !item.equals("-") && !item.equals("*") && !item.equals("/")) {
           throw new RuntimeException("Your expression `" + pol +"` contains illegal symbol: " + item);
         } else if (stackInterpret.stack.size() == 0) {                // Operator is first item
           throw new RuntimeException("Operator can't be the first element of expression. You have `" + item + "` as the first element of the input: `" + pol + "`");
         } else if (stackInterpret.stack.size() < 2) {                 // Too few items in list before operator
           throw new RuntimeException("Your expression `" + pol +"` has too few elements, can't calculate value");
         } else {
           stackInterpret.op(item);                                    // Use item as operator
           operators++;
         }
       }
     }

     if (numbers - 1 > operators) {
       throw new RuntimeException("Can't calculate expression. You have too many numbers in: `" + pol + "`");
     } else if (numbers - 1 < operators) {
       throw new RuntimeException("Can't calculate expression. You have too many operators in :" + pol);
     }
     return stackInterpret.pop();                                       // Return last item, it's the result of the expression
   }
}